#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit
{
	CLUBS,
	SPADES,
	HEARTS,
	DIAMONDS
};

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Cards
{
	Rank Rank;
	Suit Suit;
};

int main()
{
	_getch();
	return 0;
}